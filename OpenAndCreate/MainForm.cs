﻿/*Разработайте приложение, которое состоит из двух форм. Первая 
форма содержит TextBox доступный только для чтения и две кнопки 
«загрузить файл» и «редактировать». Кнопка «редактировать» изна-чально неактивна. 
При нажатии на первую кнопку, открывается диа-лог и пользователю предлагают выбрать
текстовый файл. Выбранный файл загружается в TextBox и кнопка «редактировать» 
становится ак-тивной. При нажатии на вторую кнопку открывается вторая форма 
(не модально), которая содержит TextBox доступный для редактиро-вания и две кнопки 
«Сохранить» и «Отменить». При нажатии на пер-вую кнопку изменения отображаются в TextBox первой формы */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenAndCreate
{
    public partial class MainForm : Form
    {
        EditForm edit;
        public MainForm()
        {
            edit = new EditForm(this);
            InitializeComponent();
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog.FileName;
            string fileText = System.IO.File.ReadAllText(filename);
            showTextBox.Text = fileText;
        }

        private bool Check()
        {
            if (showTextBox.Text == "")
            {
                editButton.Enabled = false;
                return false;
            }
            else
            {
                editButton.Enabled = true;
                return true;
            }
        }
        private void EditButton_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                edit.Show();
            }
        }

        private void ShowTextBox_TextChanged(object sender, EventArgs e)
        {
            Check();                   
        }

        public void ShowText(string text)
        {
            showTextBox.Text = text;
        }
 
    }
}
     